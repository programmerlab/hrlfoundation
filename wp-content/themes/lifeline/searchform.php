<form action="<?php echo home_url() ?>" class="searchform" id="search-form" method="get" role="search">
	<div class="sidebar-widget">
		<div class="sidebar-search">
			<input type="text" placeholder="<?php _e( 'Enter Search Item', SH_NAME ); ?>" class="search" id="s" name="s" value="<?php echo get_search_query(); ?>">
			<input type="submit" id="searchsubmit" value="" class="search-button">
		</div>
	</div>
</form>
